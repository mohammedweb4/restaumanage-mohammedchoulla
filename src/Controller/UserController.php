<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/users", name="users.show")
     */
    public function users(ManagerRegistry $doctrine): Response
    {
        /** it's a test **/
        $users = $doctrine->getRepository(User::class)->findAll();
        //var_dump($users);
        return $this->render(
            'user/index.html.twig',
            [
                'users' => $users
            ]
        );
    }


}
